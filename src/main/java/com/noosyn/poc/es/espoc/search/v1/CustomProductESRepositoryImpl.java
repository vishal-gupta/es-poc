package com.noosyn.poc.es.espoc.search.v1;

import java.util.List;

import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CustomProductESRepositoryImpl<T> implements CustomProductESRepository<T> {
    private final ElasticsearchOperations operations;

    @Override
    public List<T> findProductSuggestions(String keyword, Long companyId, Integer offset, Integer limit, Class<T> clazz) {
        IndexCoordinates index = IndexCoordinates.of("product_v1");
        
        NativeSearchQuery query = new NativeSearchQueryBuilder()
            .withQuery(QueryBuilders.matchQuery("completionTerms", keyword))
            .withFilter(QueryBuilders.termQuery("companyId", companyId))
            .withPageable(PageRequest.of(offset, limit))
            .build();

        SearchHits<T> hits = operations.search(query, clazz, index);
        return  hits.map(hit -> hit.getContent()).toList();
    }
    
}
