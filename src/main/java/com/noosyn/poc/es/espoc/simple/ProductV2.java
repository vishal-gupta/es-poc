package com.noosyn.poc.es.espoc.simple;

import java.util.List;

import com.noosyn.poc.es.espoc.ProductChild;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import lombok.Getter;

@Document(indexName = "product_simple")
@Getter
public class ProductV2 {
    @Id
    private Long id;
    private Long companyId;
    private String code;
    private List<ProductChild> children;
    private Long createdOn;
    private Long updatedOn; 
}
