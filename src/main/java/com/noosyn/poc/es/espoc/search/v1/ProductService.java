package com.noosyn.poc.es.espoc.search.v1;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductElasticRepository repository;
    
    public void uploadAllProducts(InputStream is) throws IOException {
        List<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(new InputStreamReader(is)).getRecords();

        List<ProductRecord> productRecords = records.stream().map(ProductRecord::of).collect(Collectors.toList());

        Map<ProductRecord, List<ProductRecord>> productGroup = productRecords.stream().collect(Collectors.groupingBy(Function.identity()));

        List<Product> products = productGroup.entrySet()
            .stream().map(Product::of)
            .collect(Collectors.toList());

        repository.saveAll(products);


    } 
}
