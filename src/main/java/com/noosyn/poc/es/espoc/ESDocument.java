package com.noosyn.poc.es.espoc;

import java.util.List;

import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import lombok.Getter;
import org.springframework.data.elasticsearch.annotations.Setting;

@Getter
@Setting(settingPath = "/settings.json")
public abstract class ESDocument {
    @Field(analyzer = "completion_analyzer", type = FieldType.Text)
    protected List<String> completionTerms;

    protected ESDocument(List<String> completionTerms) {
        this.completionTerms = completionTerms;
    }
}
