package com.noosyn.poc.es.espoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsPocApplication.class, args);
	}

}
