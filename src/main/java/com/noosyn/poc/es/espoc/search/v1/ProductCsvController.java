package com.noosyn.poc.es.espoc.search.v1;

import java.io.IOException;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/v1/productCSV")
@RequiredArgsConstructor
public class ProductCsvController {
    private final ProductService service;
    
    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> uploadProducts(@RequestPart MultipartFile file) throws IOException{
        service.uploadAllProducts(file.getInputStream());
        return ResponseEntity.ok("Success");
    }
}
