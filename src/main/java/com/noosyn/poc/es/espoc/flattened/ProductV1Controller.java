package com.noosyn.poc.es.espoc.flattened;

import java.net.URI;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/v1")
@RequiredArgsConstructor
public class ProductV1Controller {
    private final ProductV1Repository repository;

    @PostMapping("/products")
    public ResponseEntity<Long> upsertProduct(@RequestBody ProductV1 product) {
        ProductV1 aProduct = repository.save(product);

        return ResponseEntity.ok(aProduct.getId());

    }
    
}
