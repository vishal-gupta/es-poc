package com.noosyn.poc.es.espoc.nested;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/v3")
@RequiredArgsConstructor
public class ProductV3Controller {
    private final ProductV3Repository repository;

    @PostMapping("/products")
    public ResponseEntity<Long> createProduct(@RequestBody ProductV3 product) {
        ProductV3 aProduct = repository.save(product);

        return ResponseEntity.ok(aProduct.getId());
    }
    
}
