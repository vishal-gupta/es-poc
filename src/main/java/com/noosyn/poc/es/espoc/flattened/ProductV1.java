package com.noosyn.poc.es.espoc.flattened;

import java.util.List;

import com.noosyn.poc.es.espoc.ProductChild;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import lombok.Getter;

@Document(indexName = "product_flattened")
@Getter
public class ProductV1 {
    @Id
    private Long id;
    private Long companyId;
    private String code;
    @Field(type = FieldType.Flattened)
    private List<ProductChild> children;
    private Long createdOn;
    private Long updatedOn; 
}
