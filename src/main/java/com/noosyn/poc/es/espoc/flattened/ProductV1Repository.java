package com.noosyn.poc.es.espoc.flattened;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ProductV1Repository extends ElasticsearchRepository<ProductV1, Long> {
    
}