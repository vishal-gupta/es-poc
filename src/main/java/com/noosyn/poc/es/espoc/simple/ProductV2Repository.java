package com.noosyn.poc.es.espoc.simple;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ProductV2Repository extends ElasticsearchRepository<ProductV2, Long>{
    
}
