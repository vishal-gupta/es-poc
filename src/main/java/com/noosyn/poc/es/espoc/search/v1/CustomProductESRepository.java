package com.noosyn.poc.es.espoc.search.v1;

import java.util.List;

public interface CustomProductESRepository<T> {
    List<T> findProductSuggestions(String keyword, Long companyId, Integer offset, Integer limit, Class<T> clazz);
}
