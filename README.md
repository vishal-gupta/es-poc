## Elasticsearch POC With Spring Data Elasticsearch

### How to run the project
1. First you should have the docker running on your machine.
2. Then run the `docker-compose.yml` file using command `docker-compose up` (or `docker-compose up -d` for background). You may want to copy the file somewhere else so that you can run the spring appliacation from same folder.
3. Start the application using maven command `./mvnw spring-boot:run` or you can use your favourite ide as well.  
4. Upload `products_xxxxx.csv` file inside the repository using the URL from application `/v1/productsCSV`.
5. Now, you must have data in your local docker elastic container(You may check the kibana console, also running with elastic container).
6. You may check the only search API for completion search now.

> Do remember to set the correct IP of your machine in `ElasticConfig` file before run, otherwise app won't find your container.
